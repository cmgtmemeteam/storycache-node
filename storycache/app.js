var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

require("dotenv").config({ path: path.join(__dirname, ".env") });

var indexRouter = require("./routes/index");
var menuRouter = require("./routes/menu");
var readerRouter = require("./routes/reader");
var writerRouter = require("./routes/writer/writer");
var documentOverviewRouter = require("./routes/documentOverview");

var app = express();

const staticPath = path.join(__dirname, "public");

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json({ limit: "100MB" }));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(staticPath));

app.use("/", indexRouter);
app.use("/menu", menuRouter);
app.use("/reader", readerRouter);
app.use("/writer", writerRouter);
app.use("/documentOverview", documentOverviewRouter);

//catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

//error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get("env") === "development" ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render("error");
});

module.exports = app;
