const mongo = require("mongodb");
var MongoClient = mongo.MongoClient;
const Section = require("./mongo_classes").Section;
const Document = require("./mongo_classes").Document;
const connection = require("./mongo/connection");
const fallback = Object.entries(require("./fallback.json"));

function getUri(db) {
	return `mongodb+srv://${process.env.MONGODB_CLUSTER0_AUTH}@storycache0.fhfwp.mongodb.net/${db}?retryWrites=true&w=majority`;
}

const client = (db) => {
	return new MongoClient(getUri(db), {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});
};

var collectionPromise = (db, col) => {
	return new Promise((resolve, reject) => {
		var c = client(db);
		c.connect((err) => {
			// FIXME don't crash
			if (err != null) {
				c.close();
				reject(err);
			} else {
				resolve({ client: c, collection: c.db(db).collection(col) });
			}
		});
	});
};

var getMongoDocumentsPromise = (c, col, query) => {
	return new Promise((resolve, reject) => {
		col.find(query).toArray((err, docs) => {
			if (err != null) {
				c.close();
				reject(err);
			} else {
				var documents = [];
				docs.forEach((element) => {
					documents.push(new Document(element));
				});
				resolve(documents);
			}
		});
	});
};

const getDocumentPromise = (c, col, objectId) => {
	return new Promise((resolve, reject) => {
		col.find({ _id: new mongo.ObjectId(objectId) }).toArray((err, docs) => {
			if (err != null) {
				c.close();
				reject(err);
			} else if (docs.length != 1) {
				c.close();
				reject(`Docs has length ${docs.length} instead of 1`);
			} else {
				c.close();
				resolve(new Document(docs[0]));
			}
		});
	});
};

const getDocumentFromCollectionPromise = (dbName, colName, objectId) => {
	return new Promise((resolve, reject) => {
		collectionPromise(dbName, colName)
			.then((collectionResult) => {
				return getDocumentPromise(
					collectionResult.client,
					collectionResult.collection,
					objectId
				);
			})
			.then((doc) => {
				resolve(doc);
			})
			.catch((err) => {
				reject(err);
			});
	});
};

var getSectionPromise = (objectId, sectionIndex) => {
	return new Promise((resolve, reject) => {
		// TODO variable database/collection names
		collectionPromise("test_db", "test_collection")
			.then((collectionResult) => {
				return getDocumentPromise(
					collectionResult.client,
					collectionResult.collection,
					objectId
				);
			})
			.then((document) => {
				// TODO getSectionFromDocument throws
				resolve(getSectionFromDocument(document, sectionIndex));
			})
			.catch((err) => {
				reject(err);
			});
	});
};

function getSectionFromDocument(document, sectionIndex) {
	if (document.sections.length <= 0) {
		throw "No sections!";
	} else {
		return new Section(document, sectionIndex);
	}
}

function insertDocument(db, collection, document) {
	// TODO return Promise
	return new Promise((resolve, reject) => {
		connection
			.getCollection(db, collection)
			.then((collectionResult) => {
				var t = document.mongo;
				fallback.forEach((element) => {
					t[element[0]] =
						t[element[0]] !== undefined
							? t[element[0]]
							: element[1];
				});
				return new Promise((resolve, reject) => {
					collectionResult.collection
						.insertOne(t)
						.then((doc) => {
							collectionResult.client.close();
							resolve(doc.insertedId);
						})
						.catch((err) => {
							reject(err);
						});
				});
			})
			.then((id) => {
				resolve(id);
			})
			.catch((err) => {
				reject(err);
			});
	});
}

function updateDocument(dbName, collectionName, document) {
	// Does this need to return a promise?
	return new Promise((resolve, reject) => {
		connection
			.getCollection(dbName, collectionName)
			.then((colResult) => {
				var doc = document.mongo;
				doc._id = new mongo.ObjectID(doc._id);
				return new Promise((resolve, reject) => {
					colResult.collection
						.replaceOne({ _id: doc._id }, doc)
						.then((doc) => {
							resolve({
								res: doc,
								client: colResult.client,
							});
						})
						.catch((err) => {
							colResult.client.close();
							reject(err);
						});
				});
			})
			.then((doc) => {
				const res = {
					acknowledged: doc.res.acknowledged,
					matchedCount: doc.res.matchedCount,
					modifiedCount: doc.res.modifiedCount,
					upsertedId: doc.res.upsertedId,
				};
				doc.client.close();
				resolve(res);
			})
			.catch((err) => {
				console.error(`Error: ${JSON.stringify(err)}`);
				reject(err);
			});
	});
}

function insertSectionInDocument(db, collection, documentId, mongoSection) {
	connection
		.getCollection(db, collection)
		.then((collectionResult) => {
			return getDocumentPromise(
				collectionResult.client,
				collectionResult.collection,
				documentId
			);
		})
		.then((document) => {
			var newSections = [];
			document.sections.forEach((element) => {
				newSections.push(element.mongo);
			});
			newSections.push(mongoSection.mongo);
			connection
				.getCollection(db, collection)
				.then((collectionResult) => {
					return collectionResult.collection.findOneAndUpdate(
						{ _id: new mongo.ObjectID(documentId) },
						{
							$set: { sections: newSections },
						}
					);
				})
				.catch((err) => {
					console.error(err);
				});
		})
		.catch((err) => {
			console.error(err);
		});
}

exports.getMongoDocumentsPromise = getMongoDocumentsPromise;
exports.getSectionPromise = getSectionPromise;
exports.collectionPromise = collectionPromise;
exports.getDocumentPromise = getDocumentPromise;
exports.getDocumentFromCollectionPromise = getDocumentFromCollectionPromise;
exports.insertDocument = insertDocument;
exports.insertSectionInDocument = insertSectionInDocument;
exports.client = client;
exports.updateDocument = updateDocument;
