class MongoSectionContent {
	constructor(mongoJson) {
		if (mongoJson === undefined) {
			return;
		}
		this.text = mongoJson.text;
		this.image = mongoJson.image;
	}

	get mongo() {
		return {
			text: this.text,
			image: this.image,
		};
	}

	static fromJson = (json) => {
		// MongoJson and our conventions are the same
		return new MongoSectionContent(json);
	};
}

class MongoSection {
	constructor(mongoJson) {
		if (mongoJson === undefined) {
			return;
		}
		this.title = mongoJson.section_title;
		this.contents = [];
		mongoJson.contents.forEach((element) => {
			this.contents.push(new MongoSectionContent(element));
		});
		this.audio = mongoJson.audio;
	}

	get mongo() {
		var c = [];
		this.contents.forEach((element) => {
			c.push(element.mongo);
		});
		return {
			section_title: this.title,
			contents: c,
			audio: this.audio,
		};
	}

	static fromJson = (json) => {
		var s = new MongoSection();
		s.title = json.title;
		s.contents = [];
		json.contents.forEach((element) => {
			s.contents.push(MongoSectionContent.fromJson(element));
		});
		s.audio = json.audio;
		return s;
	};
}

class Section {
	constructor(document, sectionIndex) {
		if (sectionIndex < 0) {
			sectionIndex = 0;
		}
		if (sectionIndex >= document.sections.length) {
			sectionIndex = document.sections.length - 1;
		}

		var section = document.sections[sectionIndex];
		this.id = document.id;
		this.title = document.title;
		this.area = document.area;
		this.location_name = document.location_name;
		this.location_long = document.long;
		this.location_lat = document.lat;
		this.section = {
			title: section.title,
			contents: section.contents,
			audio: section.audio,
		};
		this.next_index =
			+sectionIndex + 1 < document.sections.length
				? +sectionIndex + 1
				: -1;
		this.prev_index = sectionIndex - 1;
		this.index = sectionIndex;
	}
}

class Document {
	constructor(mongoJson) {
		if (mongoJson === undefined) {
			return;
		}
		this.id = mongoJson._id;
		this.title = mongoJson.title;
		this.location_name = mongoJson.location_name;
		this.lat = mongoJson.location_lat;
		this.long = mongoJson.location_long;
		this.sections = [];
		mongoJson.sections.forEach((element) => {
			this.sections.push(new MongoSection(element));
		});
	}

	get mongo() {
		var s = [];
		this.sections.forEach((element) => {
			s.push(element.mongo);
		});
		return {
			_id: this.id,
			title: this.title,
			location_name: this.location_name,
			location_lat: this.lat,
			location_long: this.long,
			sections: s,
		};
	}

	static fromJson = (json) => {
		var d = new Document();
		d.id = json.id;
		d.title = json.title;
		d.location_name = json.location_name;
		d.lat = json.lat;
		d.long = json.long;
		d.sections = [];
		json.sections.forEach((element) => {
			d.sections.push(MongoSection.fromJson(element));
		});
		return d;
	};
}

exports.MongoSectionContent = MongoSectionContent;
exports.MongoSection = MongoSection;
exports.Document = Document;
exports.Section = Section;
