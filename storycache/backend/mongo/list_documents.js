const Document = require("../mongo_classes").Document;
const getCollection = require("./connection").getCollection;

const maxSummaryLength = 750;

function getSummary(doc) {
	var res = "";
	var s = doc.sections;
	if (s === undefined || s.length <= 0) {
		return "";
	}

	for (let i = 0; i < s.length && res.length < maxSummaryLength; i++) {
		var c = s[i].contents;
		for (let j = 0; j < c.length && res.length < maxSummaryLength; j++) {
			res += c[j].text += "\n";
		}
	}

	if (res.length > maxSummaryLength) {
		res = res.substring(0, maxSummaryLength);
		res += "...";
	}

	// REGEX MAGIC: replace multiple line breaks with one
	res.replace(/\n\s*\n/g, "\n");

	return res;
}

class DocumentStub {
	constructor(document) {
		this.id = document.id;
		this.title = document.title;
		this.location_name = document.location_name;
		this.lat = document.lat;
		this.long = document.long;
		this.section_count = document.sections.length;
		this.summary = getSummary(document);
	}

	static fromJson(json) {
		return new DocumentStub(new Document(json));
	}
}

// FIXME also allow for queries

const getAllDocumentsFromCollection = (client, collection) => {
	return new Promise((resolve, reject) => {
		collection
			.find()
			.toArray()
			.then((docs) => {
				documents = [];
				docs.forEach((element) => {
					documents.push(new Document(element));
				});
				client.close();
				resolve(documents);
			})
			.catch((err) => {
				client.close();
				reject(err);
			});
	});
};

const getAllDocuments = (dbName, collName) => {
	return new Promise((resolve, reject) => {
		getCollection(dbName, collName)
			.then((collectionResult) => {
				return getAllDocumentsFromCollection(
					collectionResult.client,
					collectionResult.collection
				);
			})
			.then((documents) => {
				resolve(documents);
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const getAllDocumentStubsFromCollection = (client, collection) => {
	return new Promise((resolve, reject) => {
		getAllDocumentsFromCollection(client, collection)
			.then((documents) => {
				var stubs = [];
				documents.forEach((element) => {
					stubs.push(new DocumentStub(element));
				});
				resolve(stubs);
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const getAllDocumentStubs = (dbName, colName) => {
	return new Promise((resolve, reject) => {
		getCollection(dbName, colName)
			.then((collectionResult) => {
				return getAllDocumentStubsFromCollection(
					collectionResult.client,
					collectionResult.collection
				);
			})
			.then((stubs) => {
				resolve(stubs);
			})
			.catch((err) => {
				reject(err);
			});
	});
};

exports.DocumentStub = DocumentStub;
exports.getAllDocuments = getAllDocuments;
exports.getAllDocumentsFromCollection = getAllDocumentsFromCollection;
exports.getAllDocumentStubs = getAllDocumentStubs;
exports.getAllDocumentStubsFromCollection = getAllDocumentStubsFromCollection;
