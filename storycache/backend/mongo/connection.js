require("dotenv").config();
const mongo = require("mongodb");
const MongoClient = mongo.MongoClient;

const getUri = (db) => {
	return `mongodb+srv://${process.env.MONGODB_CLUSTER0_AUTH}@storycache0.fhfwp.mongodb.net/${db}?retryWrites=true&w=majority`;
};

const client = (db) => {
	return new MongoClient(getUri(db), {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});
};

const collection = async (db, col) => {
	return new Promise((resolve, reject) => {
		var c = client(db);
		c.connect((err) => {
			if (err != null) {
				// Close if something didn't work -> might be unnecessary
				c.close();
				// Reject -> Something failed
				reject(err);
			} else {
				// Succeeded -> Give client (for closing later) and collection (the thing actually needed)
				resolve({ client: c, collection: c.db(db).collection(col) });
			}
		});
	});
};

exports.getCollection = collection;
