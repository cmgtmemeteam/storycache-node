const connect = require("./mongo_connect");
const docs = require("./mongo/list_documents");

exports.async = {
	getMongoDocuments: connect.getMongoDocumentsPromise,
	getSection: connect.getSectionPromise,
	collection: connect.collectionPromise,
	getDocoument: connect.getDocumentPromise,
	getDocumentFromCollection: connect.getDocumentFromCollectionPromise,
	updateDocument: connect.updateDocument,
	insertDocument: connect.insertDocument,
	getAllStubs: docs.getAllDocumentStubs,
	getAllStubsFromCollection: docs.getAllDocumentStubsFromCollection,
	getAllDocuments: docs.getAllDocuments,
	getAllDocumentsFromCollection: docs.getAllDocumentsFromCollection,
};
