const parseMongoError = (err) => {
	return { error: { status: err.message, stack: err.stack } };
};

exports.parseMongoError = parseMongoError;
