/**
 * Required External Modules
 */

/**
 *  App Configuration
 */

var express = require("express");
var router = express.Router();

/**
 * Routes Definitions
 */

router.get("/", (req, res) => {
	res.render("index", { title: "Login" });
});

module.exports = router;