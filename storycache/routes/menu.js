var express = require("express");
var router = express.Router();

router.get("/", (req, res) => {
	res.render("menu", {
		title: "Home",
		userProfile: { nickname: "Maurice" },
	});
});

module.exports = router;
