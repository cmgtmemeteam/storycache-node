var express = require("express");
var router = express.Router();
const backend = require("../../backend/backend");
const Document = require("../../backend/mongo_classes").Document;
const MongoSection = require("../../backend/mongo_classes").MongoSection;
const MongoSectionContent = require("../../backend/mongo_classes")
	.MongoSectionContent;

router.use(express.json({ limit: "100MB" }));

router.get("/", (req, res, next) => {
	res.sendFile("writer.html", { root: __dirname });
});

// TODO write a new document
// Route /writer/new
// - Creates a new document
// - Sends you to /writer/?document=<newId>

router.get("/new", (req, res, next) => {
	const randRange = (min, max) => {
		return Math.random() * (max - min) + min;
	};
	const newDoc = Document.fromJson({
		title: "New Document",
		location_name: "Enschede",
		lat: randRange(-90, 90),
		long: randRange(-180, 180),
		sections: [
			MongoSection.fromJson({
				title: "New Section",
				contents: [
					new MongoSectionContent({
						text: "Some Text",
						image: null,
					}),
				],
				audio: null,
			}),
		],
	});
	backend.async
		.insertDocument("test_db", "test_collection", newDoc)
		.then((id) => {
			res.redirect(`/writer/?document=${id}`);
		})
		.catch((err) => {
			next(err);
		});
});

router.post("/save", express.json({ limit: "100MB" }), (req, res, next) => {
	const documentId = req.query.document;
	try {
		var doc = Document.fromJson(req.body);
	} catch (e) {
		console.error(`${e.message}\n${e.stack}`);
		next(e);
		return;
	}
	if (documentId === undefined || documentId != doc.id) {
		next("ERR"); // FIXME concrete error
	} else {
		backend.async
			.updateDocument("test_db", "test_collection", doc)
			.then((_) => {
				res.send({ success: true });
			})
			.catch((err) => {
				console.error(`Error: ${err.message}\n${err.stack}`);
				next(err);
			});
	}
});

module.exports = router;
