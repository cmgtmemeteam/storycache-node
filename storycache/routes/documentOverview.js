var express = require("express");
const { async } = require("../backend/backend");
var router = express.Router();
var list_documents = require("../backend/mongo/list_documents");
var mongo_connect = require("../backend/mongo_connect");

router.get("/", (req, res) => {
	async.getAllStubs("test_db", "test_collection").then((stubs) =>{
		res.render("documentOverview", {allDocumentStubs: stubs, title: "Stories"})
	})
});


router.get("/stubs" ,(req, res, next) => {
	async.getAllStubs("test_db","test_collection").then((stubs) =>{
		res.json(stubs);
	})
	.catch((err)=>{
		next(err);
	})
})

module.exports = router;