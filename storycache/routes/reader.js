var express = require("express");
var router = express.Router();
var backend = require("../backend/backend");

var json = require("../../example.json");

router.get("/", (req, res) => {
	res.render("reader", {
		title: json.title,
		userProfile: { story: json.sections[0].contents[1].text },
	});
});

// 6023b2dbe6ec4f9d5cc9076a
// http://localhost:3000/reader/read?document=6023b2dbe6ec4f9d5cc9076a&section=0

router.get("/read", (req, res, next) => {
	const objectId = req.query.document;
	const section = req.query.section === undefined ? 0 : req.query.section;

	backend.async
		.getSection(objectId, section)
		.then((data) => {
			res.render("reader", {
				title: data.title,
				section_title: data.section.title,
				section: {
					contents: data.section.contents,
				},
				indices: {
					next: data.next_index,
					prev: data.prev_index,
					own: data.index,
				},
				document: data.id,
			});
		})
		.catch((err) => {
			next(err);
		});
});

router.get("/json", (req, res, next) => {
	const objectId = req.query.document;
	backend.async
		.getDocumentFromCollection("test_db", "test_collection", objectId)
		.then((document) => {
			res.json(document);
		})
		.catch((err) => {
			console.error(`Error: ${err}`);
			res.json(err);
			next(err);
		});
});

module.exports = router;
