$(document).ready(function () {
	$("#title-bar").visibility({
		once: false,
		onBottomPassed: function () {
			$("#title-bar-hidden").transition("fade in");
		},
		onBottomPassedReverse: function () {
			$("#title-bar-hidden").transition("fade out");
		},
	});
});
