$(() => {
        var map = L.map("map")

        L.tileLayer(
            "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            {
                attribution:
                    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            }
        ).addTo(map);

        $.get("/documentOverview/stubs", (data)=>{
            var markers = [];
            
            const myCustomColour = '#564287';
            const markerHtmlStyles = `
            background-color: ${myCustomColour};
            width: 2rem;
            height: 2rem;
            display: block;
            left: -1rem;
            top: -1rem;
            position: relative;
            border-radius: 2rem 2rem 0;
            transform: rotate(45deg);
            border: 1px solid #FFFFFF`;

            var divIcon = L.divIcon({
                className: "my-custom-pin",
                iconAnchor: [0, 24],
                labelAnchor: [-6, 0],
                popupAnchor: [0, -36],
                html: `<span style="${markerHtmlStyles}" />`
            });

            data.forEach(element => {
                markers.push(L.marker([element.lat, element.long],{icon: divIcon})
                .addTo(map)
                .bindPopup(`<a href="/reader/read?document=${element.id}" style="color: #564287;">${element.title}</a>`)
                .openPopup())
            });
            
            var group = new L.featureGroup(markers);

            map.fitBounds(group.getBounds()); 
        })

        $('#profile_card .image').dimmer({
            on: 'hover'
          });
});