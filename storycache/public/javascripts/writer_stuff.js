//#region classes
class MongoSectionContent {
	constructor(mongoJson) {
		if (mongoJson === undefined) {
			return;
		}
		this.text = mongoJson.text;
		this.image = mongoJson.image;
	}

	get mongo() {
		return {
			text: this.text,
			image: this.image,
		};
	}

	static fromJson = (json) => {
		// MongoJson and our conventions are the same
		return new MongoSectionContent(json);
	};

	get json() {
		return {
			text: this.text,
			image: this.image,
		};
	}
}

class MongoSection {
	constructor(mongoJson) {
		if (mongoJson === undefined) {
			return;
		}
		this.title = mongoJson.section_title;
		this.contents = [];
		mongoJson.contents.forEach((element) => {
			this.contents.push(new MongoSectionContent(element));
		});
		this.audio = mongoJson.audio;
	}

	get mongo() {
		var c = [];
		this.contents.forEach((element) => {
			c.push(element.mongo);
		});
		return {
			section_title: this.title,
			contents: c,
			audio: this.audio,
		};
	}

	static fromJson = (json) => {
		var s = new MongoSection();
		s.title = json.title;
		s.contents = [];
		json.contents.forEach((element) => {
			s.contents.push(MongoSectionContent.fromJson(element));
		});
		s.audio = json.audio;
		return s;
	};

	static default = () => {
		return this.fromJson({
			title: "New Section",
			contents: [
				MongoSectionContent.fromJson({
					text: "Some Text",
					image: null,
				}),
			],
			audio: null,
		});
	};

	get json() {
		var c = [];
		this.contents.forEach((element) => {
			c.push(element.json);
		});
		return {
			title: this.title,
			audio: this.audio,
			contents: c,
		};
	}
}

class Section {
	constructor(document, sectionIndex) {
		if (sectionIndex < 0) {
			sectionIndex = 0;
		}
		if (sectionIndex >= document.sections.length) {
			sectionIndex = document.sections.length - 1;
		}

		var section = document.sections[sectionIndex];
		this.id = document.id;
		this.title = document.title;
		this.area = document.area;
		this.location_name = document.location_name;
		this.location_long = document.long;
		this.location_lat = document.lat;
		this.section = {
			title: section.title,
			contents: section.contents,
			audio: section.audio,
		};
		this.next_index =
			+sectionIndex + 1 < document.sections.length
				? +sectionIndex + 1
				: -1;
		this.prev_index = sectionIndex - 1;
		this.index = sectionIndex;
	}
}

class Document {
	constructor(mongoJson) {
		if (mongoJson === undefined) {
			return;
		}
		this.id = mongoJson._id;
		this.title = mongoJson.title;
		this.location_name = mongoJson.location_name;
		this.lat = mongoJson.location_lat;
		this.long = mongoJson.location_long;
		this.sections = [];
		mongoJson.sections.forEach((element) => {
			this.sections.push(new MongoSection(element));
		});
	}

	get mongo() {
		var s = [];
		this.sections.forEach((element) => {
			s.push(element.mongo);
		});
		return {
			_id: this.id,
			title: this.title,
			location_name: this.location_name,
			location_lat: this.lat,
			location_long: this.long,
			sections: s,
		};
	}

	static fromJson = (json) => {
		var d = new Document();
		d.id = json.id;
		d.title = json.title;
		d.location_name = json.location_name;
		d.lat = json.lat;
		d.long = json.long;
		d.sections = [];
		json.sections.forEach((element) => {
			d.sections.push(MongoSection.fromJson(element));
		});
		return d;
	};

	get json() {
		var s = [];
		this.sections.forEach((element) => {
			s.push(element.json);
		});
		return {
			id: this.id,
			title: this.title,
			location_name: this.location_name,
			lat: this.lat,
			long: this.long,
			sections: s,
		};
	}
}
//#endregion classes

const titleType = "title";
const sectionTitleType = "section-title";
const sectionContentType = "section-content";

const sectionContainerId = "i-dont-know-how-to-call-this";

const inputClassName = "section-input";

const sectionClasses = " ui segment";

var editedDocument;
var inputBeingEdited;
var sectionElements = [];

function createSectionElement(index) {
	const section = editedDocument.sections[index];
	// TODO add button infront for first section
	var div = document.createElement("div");
	div.className += sectionClasses;

	var heading = document.createElement("h5");
	heading.appendChild(document.createTextNode(section.title));
	heading.dataset.sectionIndex = index;
	heading.dataset.type = sectionTitleType;
	heading.className += " editable all-content";

	initCallbacks(heading);

	div.appendChild(heading);

	var closeButton = document.createElement("button");
	closeButton.className = " ui red right corner label";
	closeButton.onclick = () => {
		var indexToRemove = sectionElements.indexOf(div);
		console.log(`INDEX: ${indexToRemove}`);
		sectionElements.splice(indexToRemove, 1);
		editedDocument.sections.splice(indexToRemove, 1);
		rebuildSections();
	};
	var closeIcon = document.createElement("i");
	closeIcon.className += " close icon";
	closeButton.appendChild(closeIcon);
	div.appendChild(closeButton);

	for (let i = 0; i < section.contents.length; i++) {
		// FIXME doesn't handle images
		var para = document.createElement("P");
		para.appendChild(document.createTextNode(section.contents[i].text));
		para.dataset.type = sectionContentType;
		para.dataset.contentIndex = i;
		para.dataset.sectionIndex = index;
		para.className += " editable section-text all-content";
		initCallbacks(para);
		div.appendChild(para);
	}
	sectionElements.splice(index, 0, div);
	return div;
}

function addSection(parent, index) {
	var div = createSectionElement(index);
	parent.appendChild(div);
	if (+index === 0) {
		addSectionAddButton(div, index - 1, true);
	}
	addSectionAddButton(div, index, false);
}

function rebuildSections() {
	var parent = document.getElementById(sectionContainerId);
	parent.textContent = "";
	buildSections(parent);
}

function buildSections(parent) {
	if (sectionElements.length <= 0) {
		if (editedDocument.sections.length <= 0) {
			editedDocument.sections.push(MongoSection.default());
		}
		for (let i = 0; i < editedDocument.sections.length; i++) {
			createSectionElement(i);
		}
	}
	if (parent === undefined) {
		var parent = document.getElementById(sectionContainerId);
	}
	for (let i = 0; i < sectionElements.length; i++) {
		let section = sectionElements[i];
		parent.appendChild(section);
		if (+i === 0) {
			addSectionAddButton(section, +i - 1, true);
		}
		addSectionAddButton(section, i, false);
	}
}

function addSectionAddButton(sectionElement, index, before) {
	const insertIndex = +index + 1;
	var div = document.createElement("div");
	div.className += " ui basic fitted segment";
	var button = document.createElement("button");
	button.className += " ui primary fluid button";
	button.appendChild(document.createTextNode("Insert Section"));
	button.onclick = () => {
		editedDocument.sections.splice(insertIndex, 0, MongoSection.default());
		createSectionElement(insertIndex);
		rebuildSections();
	};
	div.appendChild(button);
	if (before) {
		sectionElement.insertAdjacentElement("beforebegin", div);
	} else {
		sectionElement.insertAdjacentElement("afterend", div);
	}
}

function autoExpand(field) {
	// Reset field height
	field.style.height = "inherit";

	// Get the computed styles for the element
	var computed = window.getComputedStyle(field);

	// Calculate the height
	var height =
		parseInt(computed.getPropertyValue("border-top-width"), 10) +
		parseInt(computed.getPropertyValue("padding-top"), 10) +
		field.scrollHeight +
		parseInt(computed.getPropertyValue("padding-bottom"), 10) +
		parseInt(computed.getPropertyValue("border-bottom-width"), 10) -
		parseFloat(computed.fontSize);
	field.style.height = height + "px";
}

document.addEventListener(
	"input",
	function (event) {
		if (!Object.values(event.target.classList).includes(inputClassName))
			return;
		autoExpand(event.target);
	},
	false
);

const getQueryUrl = () => {
	return `/reader/json?document=${new URLSearchParams(
		window.location.search
	).get("document")}`;
};

function replaceContentTextArea(textArea) {
	// FIXME Whitespace gets combined to a single space -> now newline, etc. possible
	// For simplicity, you cannot remove a section by leaving it empty -> will not allow leaving the field
	if (textArea.dataset.type !== sectionContentType) {
		return;
	}
	if (textArea.value === undefined || textArea.value === "") {
		if (
			!Object.values(textArea.parentElement.classList).includes("error")
		) {
			textArea.parentElement.className += " error";
		}
		setTimeout(textArea.focus(), 10);
	} else {
		var sectionIndex = parseInt(textArea.dataset.sectionIndex, 10);
		var contentIndex = parseInt(textArea.dataset.contentIndex, 10);

		var para = document.createElement("P");
		para.className += " editable section-text all-content";
		para.appendChild(document.createTextNode(textArea.value));
		para.dataset.type = sectionContentType;
		para.dataset.contentIndex = contentIndex;
		para.dataset.sectionIndex = sectionIndex;

		editedDocument.sections[sectionIndex].contents[contentIndex].text =
			textArea.value;

		inputBeingEdited = undefined;

		para.dataset.type = textArea.dataset.type;
		// tA	 Input		   Form			 Whatever
		textArea.parentElement.parentElement.parentElement.insertBefore(
			para,
			// tA	 Input		   Form
			textArea.parentElement.parentElement
		);

		initCallbacks(para);
		// tA	 Input		   Form
		textArea.parentElement.parentElement.remove();
	}
}

function replaceContent(content) {
	var d = document.createElement("div");
	d.className += " ui fluid input";

	var area = document.createElement("textarea");
	area.className += inputClassName;
	area.value = content.innerText;
	area.dataset.isInput = true;
	area.dataset.type = content.dataset.type;
	area.dataset.sectionIndex = content.dataset.sectionIndex;
	area.dataset.contentIndex = content.dataset.contentIndex;
	area.rows = "0"; // No idea why but it works :>)
	d.appendChild(area);

	var form = document.createElement("div");
	form.className += " ui form";
	form.appendChild(d);

	content.parentElement.insertBefore(form, content);

	inputBeingEdited = true;

	content.remove();
	setTimeout(() => {
		area.focus();

		area.addEventListener("blur", (e) => {
			replaceElement(e.target);
		});

		area.addEventListener("keyup", (e) => {
			if (e.target.value === undefined || e.target.value === "") {
				if (
					!Object.values(e.target.parentElement.classList).includes(
						"error"
					)
				) {
					e.target.parentElement.className += " error";
				}
				setTimeout(e.target.focus(), 10);
			} else {
				e.target.parentElement.classList.remove("error");
			}
		});
	}, 10);
}

function replaceTitleInput(titleInput) {
	if (titleInput.value === undefined || titleInput.value === "") {
		if (
			!Object.values(titleInput.parentElement.classList).includes("error")
		) {
			titleInput.parentElement.className += " error";
		}
		setTimeout(titleInput.focus(), 10);
	} else {
		var h;
		if (titleInput.dataset.type === titleType) {
			h = document.createElement("h2");
			editedDocument.title = titleInput.value;
		} else if (titleInput.dataset.type === sectionTitleType) {
			h = document.createElement("h5");
			var sectionIndex = parseInt(titleInput.dataset.sectionIndex, 10);
			console.log(`Section Index: ${sectionIndex}`);
			editedDocument.sections[sectionIndex].title = titleInput.value;
			h.dataset.sectionIndex = sectionIndex;
		}

		if (h === undefined) {
			return;
		}
		h.className += " editable all-content";
		h.appendChild(document.createTextNode(titleInput.value));

		h.dataset.type = titleInput.dataset.type;

		titleInput.parentElement.parentElement.insertBefore(
			h,
			titleInput.parentElement
		);

		initCallbacks(h);
		titleInput.parentElement.remove();

		inputBeingEdited = undefined;
	}
}

function replaceTitle(title) {
	var d = document.createElement("div");
	d.className += " ui fluid input";
	d.dataset.childrenCount = "1";

	var input = document.createElement("input");
	input.type = "text";
	input.value = title.innerText;
	input.dataset.isInput = true;
	input.dataset.type = title.dataset.type;
	if (title.dataset.sectionIndex !== undefined) {
		input.dataset.sectionIndex = title.dataset.sectionIndex;
	}
	d.appendChild(input);

	title.parentElement.insertBefore(d, title);

	title.remove();

	inputBeingEdited = true;
	setTimeout(() => {
		input.focus();

		input.addEventListener("blur", (e) => {
			replaceElement(e.target);
		});

		input.addEventListener("keyup", (e) => {
			if (e.target.value === undefined || e.target.value === "") {
				if (
					!Object.values(e.target.parentElement.classList).includes(
						"error"
					)
				) {
					e.target.parentElement.className += " error";
				}
				setTimeout(e.target.focus(), 10);
			} else {
				e.target.parentElement.classList.remove("error");
			}
		});
	}, 10);
}

function replaceElement(element) {
	// TODO prevent clicking on other items while an item is being edited
	if (element.dataset.isInput === "true") {
		// Replace input with plain text
		if (
			element.dataset.type === titleType ||
			element.dataset.type === sectionTitleType
		) {
			replaceTitleInput(element);
		} else if (element.dataset.type === sectionContentType) {
			replaceContentTextArea(element);
		}
	} else {
		// TODO don't replace if another thing is being edited
		if (inputBeingEdited !== undefined) {
			// There is something being edited
			return;
		}
		// Replace plain text with input
		if (
			element.dataset.type === titleType ||
			element.dataset.type === sectionTitleType
		) {
			replaceTitle(element);
		} else if (element.dataset.type === sectionContentType) {
			replaceContent(element);
			expandAllInputs();
		}
	}
}

const initCallbacks = (element) => {
	element.addEventListener("mousedown", (e) => {
		replaceElement(e.target);
	});
};

function saveDocument(element) {
	addClassIfNotAdded(element, "loading");

	$.ajax({
		url: `/writer/save?document=${editedDocument.id}`,
		type: "POST",
		contentType: "application/json",
		data: JSON.stringify(editedDocument),
		complete: () => {
			element.classList.remove("loading");
		},
		error: (xhr, ajaxOptions, thrownError) => {
			alert(`Failed!\n${thrownError}\n`);
		},
	});
}

function saveAndReadDocument(element) {
	addClassIfNotAdded(element, "loading");

	$.ajax({
		url: `/writer/save?document=${editedDocument.id}`,
		type: "POST",
		contentType: "application/json",
		data: JSON.stringify(editedDocument),
		complete: () => {
			element.classList.remove("loading");
		},
		success: () => {
			readDoc();
		},
		error: (xhr, ajaxOptions, thrownError) => {
			alert(`Failed!\n${thrownError}\n`);
		},
	});
}

function reloadOld() {
	var sections = document.getElementById("section-container");
	sections.textContent = "";

	var buttons = document.getElementById("save-load-buttons");
	addClassIfNotAdded(buttons, "hide-element");

	loadDoc(buttons);
}

function loadDoc(buttons) {
	$.get(getQueryUrl(), (data) => {
		var contentContainer = document.getElementById("section-container");

		var h = document.createElement("h2");
		h.className += " editable all-content";
		h.appendChild(document.createTextNode(data.title));
		h.dataset.type = titleType;
		contentContainer.appendChild(h);
		initCallbacks(h);

		var div = document.createElement("div");
		div.id = sectionContainerId;
		contentContainer.appendChild(div);

		editedDocument = data;

		sectionElements = [];
		buildSections();

		buttons.classList.remove("hide-element");
	});
}

function expandAllInputs() {
	var inputs = document.getElementsByClassName(inputClassName);
	Object.values(inputs).forEach((element) => {
		autoExpand(element);
	});
}

$(() => {
	expandAllInputs();

	var buttons = document.getElementById("save-load-buttons");
	addClassIfNotAdded(buttons, "hide-element");

	loadDoc(buttons);
});

function addClassIfNotAdded(element, cssClass) {
	if (!Object.values(element).includes(cssClass)) {
		element.className += " " + cssClass;
	}
}

function readDoc() {
	console.log(`ID: ${document.id}`);
	location.href = `/reader/read?document=${new URLSearchParams(
		window.location.search
	).get("document")}`;
}
